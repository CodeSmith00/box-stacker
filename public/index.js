"use strict";

let game;

function onload() {

    function newGame() {
        const config = {
            type: Phaser.CANVAS,
            canvas: document.getElementById("phasercanvas"),
            width: resolution,
            height: resolution,
            scale: {
                mode: Phaser.Scale.FIT,
                autoCenter: Phaser.Scale.CENTER_BOTH,
                width: resolution,
                height: resolution
            },
            scene: [GameScene]
        };

        return new Phaser.Game(config);
    }

    game = newGame();
}

window.addEventListener('load', onload);
