"use strict";

const BACKGROUND_PNG_SIZE = 500;
const BALL_SVG_SIZE = 175;
const LESS_DETAIL_BUTTON_SVG_SIZE = 450;
const MINUS_SVG_SIZE = 100;
const MENU_BUTTON_SVG_SIZE = 100;
const MENU_BACKGROUND_SVG_SIZE = 500;
const MORE_DETAIL_BUTTON_SVG_SIZE = 450;
const PLUS_SVG_SIZE = 100;

const GameScene = new Phaser.Class({
    Extends: Phaser.Scene,

    sim_on: false,

    initialize: function GameScene() {
        Phaser.Scene.call(this, { key: 'GameScene', active: true });
    },

    preload: function() {
        //this.load.image('background', 'images/background.png');
        this.load.image('ball', 'images/ball.svg');
        this.load.json('shape_def', 'shape_def.json');
        this.load.json('level', 'level.json');
        this.load.json('dynamic_object_def', 'dynamic_object_def.json');
    },

    create: function() {
        const sd_result = this.cache.json.get('shape_def');
        shapeDefs = jsonShapeDefsToJS(sd_result);
        const level = this.cache.json.get('level');
        const dynObjDefs_result = this.cache.json.get('dynamic_object_def');

        this.input.addPointer();

        this.add.image(
            resolution / 2, resolution / 2, 'background'
        ).setScale(resolution / BACKGROUND_PNG_SIZE);

        /*
        // Ball

        const ballX = 8;
        const ballY = 2;
        const ballR = 1;

        const ballPhaserObject = this.add.image(
            ((ballX - ballR) / LEVEL_SIZE) * resolution,
            ((ballY - ballR) / LEVEL_SIZE) * resolution,
            'ball'
        ).setScale(((ballR * 2) / LEVEL_SIZE) * resolution / BALL_SVG_SIZE);

        const ballPlanckObject = this.planck.add.circle(
            ballPhaserObject,
            ballX,
            ballY,
            ballR,
            {
                density: 1.0,
                friction: 0.2
            }
        );

        ballPlanckObject.setLinearVelocity(Vec2(1.0, 10.02));
        //ballPlanckObject.setBounce(0.9, 0.9);
        */

        this.keySpace = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);

        setup(level, this, dynObjDefs_result);

        this.cameras.main.setZoom(1);
        this.cameras.main.setBounds(0, 0, resolution, resolution);

        let pointer1 = null;
        let pointer2 = null;

        this.input.on("pointerdown", function (pointer) {
            if (!this.sim_on && !this.finished) {
                place_block(this, pointer);
            }
        }, this);

        const maybePinchZoom = () => {
            if (pointer1 && pointer1.isDown && pointer2 && pointer2.isDown) {

                const prevDist = pointer1.prevPosition.distance(
                    pointer2.prevPosition);

                const newDist = pointer1.position.distance(pointer2.position);

                const zoomFactor = newDist/prevDist;

                if (
                    Number.isFinite(zoomFactor)
                    && Math.abs(zoomFactor - 1) < 0.1
                ) {
                    this.events.emit("zoom", zoomFactor);
                }
            }
        };

        this.input.on('pointermove', (pointer, _currentlyOver) => {
            if (pointer.isDown) {
                const dx = pointer.position.x - pointer.prevPosition.x;
                const dy = pointer.position.y - pointer.prevPosition.y;
                const cam = this.cameras.main;
                const scale = this.cameras.main.zoom;
                cam.scrollX -= dx / scale;
                cam.scrollY -= dy / scale;
            }

            if (pointer.id === 1) {
                pointer1 = pointer;
                maybePinchZoom();
            } else if (pointer.id === 2) {
                pointer2 = pointer;
                maybePinchZoom();
            }
        });

        this.events.on('zoom', (amount) => {
            let z = this.cameras.main.zoom * amount;
            if (z > 4) {
                z = 4;
            } else if (z < 1) {
                z = 1;
            }
            this.cameras.main.zoom = z;
        });

        this.events.on('detail', (newResolution) => {
            if (newResolution > 1000) {
                newResolution = 1000;
            } else if (newResolution < 100) {
                newResolution = 100;
            }

            if (resolution === newResolution) {
                return;
            }

            if (window.confirm("This will restart your game. Are you sure?")) {
                game.destroy(true, false);
                resolution = newResolution;
                game = newGame();
                // TODO: save detail level to local storage
            }
        });
    },

    update: function() {

        if (this.keySpace.isDown) {
            if (!this.sim_on && !this.finished) {
                joinTiles();
                this.sim_on = true;
                this.finished = false;
            }
        }

        if (this.sim_on) {
            update(this);
        }
        // TODO: world.clearForces() ?

        for (let b = world.getBodyList(); b; b = b.getNext()) {
            let bodyPosition = b.getPosition();
            let bodyAngle = b.getAngle();
            let phaserObject = b.getUserData();
            if (phaserObject) {
                phaserObject.x = planckToPhaserX(bodyPosition.x);
                phaserObject.y = planckToPhaserY(bodyPosition.y);
                phaserObject.rotation = bodyAngle;
            }
        }
    },

    screenToPlanck: function(screenPos) {
        const worldPoint = this.cameras.main.getWorldPoint(
            screenPos.x,
            screenPos.y
        );
        return new Vec2(
            ( worldPoint.x / resolution ) * 20,
            ( worldPoint.y / resolution ) * 20
        );
    },
});

function phaserColour(colourString) {
    return (
        (parseInt(colourString.substr(1,1), 16) * 16 * 16 * 16 * 16 * 16) +
        (parseInt(colourString.substr(2,1), 16) * 16 * 16 * 16) +
        (parseInt(colourString.substr(3,1), 16) * 16)
    );
}

function planckToPhaserX(planckX) {
    return (planckX / 20) * resolution;
}

function planckToPhaserY(planckY) {
    return (planckY / 20) * resolution;
}

function planckToPhaserW(planckW) {
    return (planckW / 20) * resolution;
}

function planckToPhaserH(planckH) {
    return (planckH / 20) * resolution;
}
