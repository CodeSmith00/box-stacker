(function() {

let phasercanvas;
let overlay;
let menu;
let menu_items;

let menuVisible = false;

function onload() {
    phasercanvas = document.getElementById('phasercanvas');
    overlay = document.getElementById('overlay');
    menu = document.getElementById("menu");

    menu_items = [
        {
            x: 0,
            y: 0,
            w: 8,
            h: 1,
            type: 'label',
            text: 'Level 1',
        },
        {
            x: 7,
            y: 0,
            w: 1,
            h: 1,
            type: 'button',
            icon: 'menu.svg',
            callback: toggleMenu
        },
        {
            x: 0,
            y: 7,
            w: 8,
            h: 1,
            type: 'label',
            text: 'Score: 27'
        },
        {
            x: 7,
            y: 7,
            w: 1,
            h: 1,
            type: 'button',
            text: '-',
            callback: zoomOut,
        },
        {
            x: 7,
            y: 6,
            w: 1,
            h: 1,
            type: 'button',
            text: '+',
            callback: zoomIn,
        }
    ]

    updateMenu(menu_items);

    onresize();
}

function toggleMenu() {
    menuVisible = !menuVisible;

    let menuItems;
    if (menuVisible) {
        menuItems = [
            {
                x: 4,
                y: 1,
                w: 5,
                h: 1,
                type: 'button',
                text: 'Item 1',
                callback: () => changeCurrentShape(0)
            },
            {
                x: 4,
                y: 2,
                w: 5,
                h: 1,
                type: 'button',
                text: 'Item 2',
                callback: () => changeCurrentShape(1)
            },
            {
                x: 4,
                y: 3,
                w: 5,
                h: 1,
                type: 'button',
                text: 'Item 3',
                callback: () => changeCurrentShape(2)
            },
            {
                x: 7,
                y: 0,
                w: 1,
                h: 1,
                type: 'button',
                icon: 'menu.svg',
                callback: toggleMenu
            }
        ];
    } else {
        menuItems = menu_items;
    }

    menuItems.push();

    updateMenu(menuItems);
}

function zoomIn() {
    game.scene.scenes[0].events.emit('zoom', 1.2);
}

function zoomOut() {
    game.scene.scenes[0].events.emit('zoom', 1 / 1.2);
}

function onresize() {
    // Wait until all canvas resize logic has run, then we copy it
    setTimeout(doresize, 0);
}

function doresize() {
    overlay.style.marginLeft = phasercanvas.style.marginLeft;
    overlay.style.marginTop = phasercanvas.style.marginTop;
    overlay.style.width = phasercanvas.style.width;
    overlay.style.height = phasercanvas.style.height;
}

function createLabel(item) {
    const label = document.createElement('span');
    if (item.text) {
        label.innerText = item.text;
    }
    if (item.icon) {
        const img = document.createElement('img');
        img.src = `images/${item.icon}`;
        label.appendChild(img);
    }
    label.style.gridColumnStart = item.x + 1;
    label.style.gridColumnEnd = item.x + item.w + 1;
    label.style.gridRowStart = item.y + 1;
    label.style.gridRowEnd = item.y + item.h + 1;
    return label;
}

function createButton(item) {
    const button = document.createElement('button');
    if (item.text) {
        button.innerText = item.text;
    }
    if (item.icon) {
        const img = document.createElement('img');
        img.src = `images/${item.icon}`;
        button.appendChild(img);
    }
    button.style.gridColumnStart = item.x + 1;
    button.style.gridColumnEnd = item.x + item.w + 1;
    button.style.gridRowStart = item.y + 1;
    button.style.gridRowEnd = item.y + item.h + 1;
    button.onclick = item.callback;
    return button;
}

function deleteAllChildren(element) {
    while (element.firstChild) {
      element.removeChild(element.firstChild);
    }
}

function updateMenuSection(menuSection, overlaySection, x, direction) {
    if (!menuSection) {
        return;
    }

    deleteAllChildren(overlaySection);

    let y = direction > 0 ? 1 : 8;
    for (let item of menuSection) {
        let el;
        if (item.type === 'label') {
            el = createLabel(item, x, y);
        } else if (item.type === 'button') {
            el = createButton(
                item,
                item.x ?? x,
                item.y ?? y,
                item.xwidth ?? 0
            );
        }
        if (el) {
            overlaySection.appendChild(el);
        }
        y += direction;
    }
}

function updateMenu(menuDef) {
    // updateMenuSection(menuDef.topLeft, overlayTl, 0, 1);
    // updateMenuSection(menuDef.topRight, overlayTr, 8, 1);
    // updateMenuSection(menuDef.bottomLeft, overlayBl, 0, -1);
    // updateMenuSection(menuDef.bottomRight, overlayBr, 8, -1);

    deleteAllChildren(menu);

    for (let item of menuDef) {
        let el;
        if (item.type === "label") {
            el = createLabel(item)
        } else if (item.type === "button") {
            el = createButton(item)
        }
        if (el) {
            menu.appendChild(el);
        }
    }
}

window.addEventListener('load', onload);
window.addEventListener('resize', onresize);

})();
