"use strict";

// form types enum
let FORM_TYPE = {
    Tri: "Tri",
    Rect: "Rect",
    Circle: "Circle",
    Polygon: "Polygon",
};

let Form = class {
    constructor(type, data) {
        this.type = type;

        if (type === "Tri") {
            this.pos1 = data[0];
            this.pos2 = data[1];
            this.pos3 = data[2];
        } else if (type === "Rect") {
            this.corner1 = data[0];
            this.corner2 = Vec2(data[0].x, data[1].y);
            this.corner3 = data[1];
            this.corner4 = Vec2(data[1].x, data[0].y);
        } else if (type === "Circle") {
            this.centre = data[0];
            this.radius = data[1];
        } else if (type === "Polygon") {
            this.points = data;
        }
    }
};

// Define an Area
let Area = class {
    constructor(form) {
        this.form = form;
        this.type = form.type;
        if (form.type == FORM_TYPE.Rect) {
            this.x1 = form.corner1.x;
            this.x2 = form.corner3.x;
            this.y1 = form.corner1.y;
            this.y2 = form.corner3.y;
        }
    }
};

// Block and Object Classes
let StaticBlock = class {
    constructor(x, y, w, h, object) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        this.colour = "#777";

        this.object = object;
    }
};

let ShapeBlock = class {
    constructor(x, y, type, object) {
        this.x = x;
        this.y = y;

        this.type = type;
        this.colour = shapeDefs[type][1];

        this.object = object;
    }
};

let Car = class {
    constructor(x, y, id, car, joint) {
        this.x = x;
        this.y = y;
        this.id = id;

        this.car = car;
        this.joint = joint;
    }
};

// Get value out of var
function getValue(value) {
    if (typeof value != Number) {
        if (value == "time") {
            return simTick;
        }
    }
    return value;
}

// Condition Classes
let greaterThan = class {
    constructor(num1, num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    isTrue() {
        let num1 = getValue(this.num1);
        let num2 = getValue(this.num2);

        if (num1 > num2) {
            return true;
        } else {
            return false;
        }
    }
};

let lessThan = class {
    constructor(num1, num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    isTrue() {
        let num1 = getValue(this.num1);
        let num2 = getValue(this.num2);

        if (num1 < num2) {
            return true;
        } else {
            return false;
        }
    }
};

let Equals = class {
    constructor(num1, num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    isTrue() {
        let num1 = getValue(this.num1);
        let num2 = getValue(this.num2);

        if (num1 == num2) {
            return true;
        } else {
            return false;
        }
    }
};

let bodyEnteredArea = class {
    constructor(area, body) {
        this.area = area;
        this.body = body;
    }

    isTrue(time) {
        let position = this.body.getPosition();

        let inside;
        if (this.area.type === FORM_TYPE.Rect) {
            if (
                position.x >= this.area.x1 &&
                position.x <= this.area.x2 &&
                position.y >= this.area.y1 &&
                position.y <= this.area.y2
            ) {
                inside = true;
            } else {
                inside = false;
            }
        }

        return inside;
    }
};

function jsonConditionsToObjects(conditions) {
    let conditions_formatted = [];

    for (let condition of conditions) {
        if (condition.always != undefined) {
            conditions_formatted.push(condition.always);
        } else if (condition.greaterThan) {
            conditions_formatted.push(
                new greaterThan(
                    condition.greaterThan[0],
                    condition.greaterThan[1]
                )
            );
        } else if (condition.lessThan) {
            conditions_formatted.push(
                new lessThan(
                    condition.lessThan[0],
                    condition.lessThan[1]
                )
            );
        } else if (condition.Equals) {
            conditions_formatted.push(
                new Equals(
                    condition.Equals[0],
                    condition.Equals[1]
                )
            );
        } else if (condition.bodyEnteredArea) {
            let area = new Area(parseForm(condition.bodyEnteredArea[0]));

            let body;
            if (condition.bodyEnteredArea[1] == "main_car") {
                if (!main_car) {
                    console.error("No car in the level");
                    conditions_formatted.push(false);
                    continue;
                } else {
                    console.log(body);
                    body = main_car.car;
                }
            } else {
                for (let dynObj of dynamic_objects) {
                    if (condition.bodyEnteredArea[1] === dynObj.id) {
                        if (dynObj.car)
                        body = dynObj.car;
                    }
                }
            }

            conditions_formatted.push(new bodyEnteredArea(area, body));
        }
    }

    return conditions_formatted;
}

function jsonShapeDefsToJS(shape_defs) {
    let def_list = [];
    let pos_defs = [];
    for (let shape_def of shape_defs) {
        pos_defs = [];
        for (let pos of shape_def.tiles) {
            pos_defs.push([Vec2(pos.x, pos.y), pos.type]);
        }
        def_list.push([pos_defs, shape_def.colour]);
    }

    return def_list;
}

function jsonDynObjDefsToJS(dynamic_objects_defs) {
    let def_list = [];

    for (let def of dynamic_objects_defs) {
        if (def.type === "car") {
            def.body = parseForm(def.body);
            def.wheel1 = parseForm(def.wheel1);
            def.wheel2 = parseForm(def.wheel2);

            def_list.push(def);
        }
    }

    return def_list;
}

// Parse Json to create forms
function parseForm(json) {
    if (json.TRI) {
        return new Form(FORM_TYPE.Tri, [
            Vec2(json.TRI[0].x, json.TRI[0].y),
            Vec2(json.TRI[1].x, json.TRI[1].y),
            Vec2(json.TRI[2].x, json.TRI[2].y),
        ]);
    } else if (json.RECT) {
        return new Form(FORM_TYPE.Rect, [
            Vec2(json.RECT[0].x, json.RECT[0].y),
            Vec2(json.RECT[1].x, json.RECT[1].y),
        ]);
    } else if (json.CIRCLE) {
        return new Form(FORM_TYPE.Circle, [
            Vec2(json.CIRCLE[0].x, json.CIRCLE[0].y),
            json.CIRCLE[1],
        ]);
    } else if (json.POLYGON) {
        let points = [];
        for (let i of json.POLYGON) {
            points.push(Vec2(i.x, i.y));
        }

        return new Form(FORM_TYPE.Polygon, points);
    }
}

// Check Condition
function checkCondition(conditions) {
    let correct = true;

    for (let condition of conditions) {
        if (typeof condition == "boolean") {
            if (!condition) {
                correct = false;
            }
        } else if (!condition.isTrue()) {
            correct = false;
        }
    }

    return correct;
}

// The starting model
function startModel() {
    return {
        sim_on: false,
    };
}

function createPlanckObjFromForm(form, body_pos, obj_density, obj_friction) {
    let obj;

    if (form.type === FORM_TYPE.Tri) {
        let pos1 = Vec2(form.pos1.x + body_pos.x, form.pos1.y + body_pos.y);
        let pos2 = Vec2(form.pos2.x + body_pos.x, form.pos2.y + body_pos.y);
        let pos3 = Vec2(form.pos3.x + body_pos.x, form.pos3.y + body_pos.y);

        let centre = Vec2(
            (pos1.x + pos2.x + pos3.x) / 3.0,
            (pos1.y + pos2.y + pos3.y) / 3.0
        );
        let def = {
            type: "dynamic",
            position: centre,
        };

        obj = world.createBody(def);

        let obj_shape = planck.Polygon([pos1, pos2, pos3]);

        let fixture = {
            shape: obj_shape,
            density: obj_density,
            friction: obj_friction,
        };

        obj.createFixture(fixture);
    } else if (form.type === FORM_TYPE.Rect) {
        let x1 = form.corner1.x + body_pos.x;
        let x2 = form.corner3.x + body_pos.x;
        let y1 = form.corner1.y + body_pos.y;
        let y2 = form.corner3.y + body_pos.y;

        let centre = Vec2((x1 + x2) / 2.0, (y1 + y2) / 2.0);
        let def = {
            type: "dynamic",
            position: centre,
        };

        obj = world.createBody(def);

        let half_size = Vec2(
            Math.abs((x2 - x1) / 2.0),
            Math.abs((y2 - y1) / 2.0)
        );

        let obj_shape = planck.Box(half_size.x, half_size.y);

        let fixture = {
            shape: obj_shape,
            density: obj_density,
            friction: obj_friction,
        };

        obj.createFixture(fixture);
    } else if (form.type === FORM_TYPE.Circle) {
        let centre = Vec2(
            form.centre.x + body_pos.x,
            form.centre.y + body_pos.y
        );

        let def = {
            type: "dynamic",
            position: centre,
        };

        obj = world.createBody(def);

        let obj_shape = planck.Circle(Vec2(0, 0), form.radius);

        let fixture = {
            shape: obj_shape,
            density: obj_density,
            friction: obj_friction,
        };

        obj.createFixture(fixture);
    }

    return obj;
}

function createDynObj(gameScene, def, data) {
    if (def.type === "car") {
        return createCar(
            gameScene,
            Vec2(data.x, data.y),
            data.id,
            def.body_density,
            def.body_friction,
            def.body,
            def.wheel1_density,
            def.wheel1_friction,
            def.wheel1,
            def.wheel2_density,
            def.wheel1_friction,
            def.wheel2,
            def.joint1_pos,
            def.joint2_pos
        );
    }
}

// Create a car
function createCar(
    gameScene,
    pos,
    id,
    b_density,
    b_friction,
    b_shape,
    w1_density,
    w1_friction,
    w1_shape,
    w2_density,
    w2_friction,
    w2_shape,
    j1_pos,
    j2_pos
) {
    // let wheel1Def = {
    //   type: "dynamic",
    //   position: Vec2(pos.x - b_size.x / 2, pos.y),
    // };

    // let wheel2Def = {
    //   type: "dynamic",
    //   position: Vec2(pos.x + b_size.x / 2, pos.y),
    // };

    // let boxDef = {
    //   type: "dynamic",
    //   position: Vec2(pos.x, pos.y - b_size.y / 2),
    // };

    const w_radius = w1_shape.radius;
    const b_size = {
        x: b_shape.corner3.x - b_shape.corner1.x,
        y: b_shape.corner3.y - b_shape.corner1.y
    };

    let wheel1Def = {
        type: "dynamic",
        position: Vec2(pos.x - b_size.x / 2, pos.y),
    };

    let wheel2Def = {
       type: "dynamic",
        position: Vec2(pos.x + b_size.x / 2, pos.y),
    };

    let boxDef = {
        type: "dynamic",
        position: Vec2(pos.x, pos.y - b_size.y / 2),
    };

    // let wheel1 = world.createBody(wheel1Def);
    // let wheel2 = world.createBody(wheel2Def);

    // let box = world.createBody(boxDef);

    // let wheelShape = planck.Circle(Vec2(0, 0), w_radius);

    // let boxShape = planck.Box(b_size.x / 2, b_size.y / 2);

    // let wheelFixtureDef = {
    //   shape: wheelShape,
    //   density: w_density,
    //   friction: 0.2,
    // };

    // let boxFixtureDef = {
    //   shape: boxShape,
    //   density: b_density,
    //   friction: 0.2,
    // };

    // wheel1.createFixture(wheelFixtureDef);
    // wheel2.createFixture(wheelFixtureDef);

    // box.createFixture(boxFixtureDef);

    let box = createPlanckObjFromForm(b_shape, pos, b_density, b_friction);
    let wheel1 = createPlanckObjFromForm(
        w1_shape,
        pos,
        w1_density,
        w1_friction
    );
    let wheel2 = createPlanckObjFromForm(
        w2_shape,
        pos,
        w2_density,
        w2_friction
    );

    const phaserWheel1 = gameScene.add.circle(
        planckToPhaserX(wheel1Def.position.x),
        planckToPhaserY(wheel1Def.position.y),
        planckToPhaserW(w_radius),
        phaserColour("#080")
    );
    const phaserWheel2 = gameScene.add.circle(
        planckToPhaserX(wheel2Def.position.x),
        planckToPhaserY(wheel2Def.position.y),
        planckToPhaserW(w_radius),
        phaserColour("#080")
    );
    const phaserBox = gameScene.add.rectangle(
        planckToPhaserX(boxDef.position.x),
        planckToPhaserY(boxDef.position.y),
        planckToPhaserW(b_size.x),
        planckToPhaserH(b_size.y),
        phaserColour("#080")
    );

    wheel1.setUserData(phaserWheel1);
    wheel2.setUserData(phaserWheel2);
    box.setUserData(phaserBox);

    bodies.push(wheel1);
    bodies.push(wheel2);
    bodies.push(box);

    let joint1 = world.createJoint(
        planck.RevoluteJoint(
            {
                motorSpeed: 50.0,
                maxMotorTorque: 50.0,
                enableMotor: true,
                frequencyHz: 4.0,
                dampingRatio: 0.7,
            },
            box,
            wheel1,
            Vec2(j1_pos.x + pos.x, j1_pos.y + pos.y)
        )
    );
    let joint2 = world.createJoint(
        planck.RevoluteJoint(
            {},
            box,
            wheel2,
            Vec2(j2_pos.x + pos.x, j2_pos.y + pos.y)
        )
    );

    joints.push(joint1);

    let car_object = new Car(pos.x, pos.y, id, box, joint1);

    return car_object;
}

// Create a joint
function createJoint(body1, body2, jointPos) {
    let joint = world.createJoint(planck.WeldJoint({}, body1, body2, jointPos));

    joints.push(joint);

    return joint;
}

// Create a Shape
function createShape(gameScene, unformatted_position, shape_id, orientation = [0, false]) {
    let position = Vec2(
        unformatted_position.x + 0.5,
        unformatted_position.y + 0.5
    );

    let bodyDef = {
        type: "dynamic",
        position: position,
    };
    let box = world.createBody(bodyDef);

    box.type = shape_id;

    let this_shape_def = [[], shapeDefs[shape_id][1]];

    for (let a of shapeDefs[shape_id][0]) {
        this_shape_def[0].push([Vec2(a[0].x, a[0].y), a[1]]);
    }

    for (let i = 0; i < this_shape_def[0].length; i++) {
        if (orientation[1]) {
            this_shape_def[0][i] = [
                Vec2(-this_shape_def[0][i][0].x, this_shape_def[0][i][0].y),
                this_shape_def[0][i][1],
            ];
        }

        if (orientation[0] === 90) {
            this_shape_def[0][i] = [
                Vec2(-this_shape_def[0][i][0].y, this_shape_def[0][i][0].x),
                this_shape_def[0][i][1],
            ];
        } else if (orientation[0] === 180) {
            this_shape_def[0][i] = [
                Vec2(-this_shape_def[0][i][0].x, -this_shape_def[0][i][0].y),
                this_shape_def[0][i][1],
            ];
        } else if (orientation[0] === 270) {
            this_shape_def[0][i] = [
                Vec2(this_shape_def[0][i][0].y, -this_shape_def[0][i][0].x),
                this_shape_def[0][i][1],
            ];
        }
    }

    let phaserContainer = gameScene.add.container(
        planckToPhaserX(position.x),
        planckToPhaserY(position.y),
        []
    );
    box.setUserData(phaserContainer);

    const colour = shapeDefs[shape_id][1];
    for (let shapeDef of this_shape_def[0]) {
        let dynamicBox;
        if (shapeDef[1] === "full") {
            let phaserRectangle = gameScene.add.rectangle(
                planckToPhaserX(shapeDef[0].x),
                planckToPhaserY(shapeDef[0].y),
                planckToPhaserW(1),
                planckToPhaserH(1),
                phaserColour(colour)
            );
            phaserContainer.add(phaserRectangle);

            dynamicBox = planck.Box(0.5, 0.5, shapeDef[0], 0.0);
        } else if (shapeDef[1] === "slope") {
            let modifiers = [
                Vec2(orientation[1] ? 0.5 : -0.5, -0.5),
                Vec2(orientation[1] ? 0.5 : -0.5, 0.5),
                Vec2(orientation[1] ? -0.5 : 0.5, 0.5),
            ];

            if (orientation[0] === 90) {
                modifiers = [
                    Vec2(-modifiers[0].y, modifiers[0].x),
                    Vec2(-modifiers[1].y, modifiers[1].x),
                    Vec2(-modifiers[2].y, modifiers[2].x),
                ];
            } else if (orientation[0] === 180) {
                modifiers = [
                    Vec2(-modifiers[0].x, -modifiers[0].y),
                    Vec2(-modifiers[1].x, -modifiers[1].y),
                    Vec2(-modifiers[2].x, -modifiers[2].y),
                ];
            } else if (orientation[0] === 270) {
                modifiers = [
                    Vec2(modifiers[0].y, -modifiers[0].x),
                    Vec2(modifiers[1].y, -modifiers[1].x),
                    Vec2(modifiers[2].y, -modifiers[2].x),
                ];
            }

            let vertices = [
                Vec2(
                    shapeDef[0].x + modifiers[0].x,
                    shapeDef[0].y + modifiers[0].y
                ),
                Vec2(
                    shapeDef[0].x + modifiers[1].x,
                    shapeDef[0].y + modifiers[1].y
                ),
                Vec2(
                    shapeDef[0].x + modifiers[2].x,
                    shapeDef[0].y + modifiers[2].y
                ),
            ];

            let vertices_phaser = [];
            for (let p of vertices) {
                vertices_phaser.push(Vec2(planckToPhaserX(p.x), planckToPhaserY(p.y)));
            }

            let phaserSlope = gameScene.add.polygon(
                planckToPhaserX(shapeDef[0].x + 0.5),
                planckToPhaserY(shapeDef[0].y + 0.5),
                vertices_phaser,
                phaserColour(colour)
            );
            phaserContainer.add(phaserSlope);

            dynamicBox = planck.Polygon(vertices);
        }

        let fixtureDef = {
            shape: dynamicBox,
            density: 1.0,
            friction: 0.2,
        };

        box.createFixture(fixtureDef);
        collisions_array.push([
            position.x - 0.5 + shapeDef[0].x,
            position.y - 0.5 + shapeDef[0].y,
        ]);

        tiles[position.y - 0.5 + shapeDef[0].y][
            position.x - 0.5 + shapeDef[0].x
        ] = bodies.length;
    }

    bodies.push(box);

    return new ShapeBlock(
        unformatted_position.x,
        unformatted_position.y,
        shape_id,
        box
    );
}

// Create a ground body
function createGround(gameScene, pos, size) {
    let halfSize = Vec2(size.x / 2, size.y / 2);

    let centrePos = Vec2(pos.x + halfSize.x, pos.y + halfSize.y);

    let groundBodyDef = {
        position: centrePos,
    };

    let groundBody = world.createBody(groundBodyDef);

    let phaserBody = gameScene.add.rectangle(
        planckToPhaserX(pos.x),
        planckToPhaserY(pos.y),
        planckToPhaserW(size.x),
        planckToPhaserH(size.y),
        phaserColour("#777")
    );
    groundBody.setUserData(phaserBody);

    let groundBox = planck.Box(halfSize.x, halfSize.y);

    groundBody.createFixture(groundBox, 0.0);

    for (let y = 0; y < size.y; y++) {
        for (let x = 0; x < size.x; x++) {
            collisions_array.push([
                x + (centrePos.x - halfSize.x),
                y + (centrePos.y - halfSize.y),
            ]);
        }
    }

    bodies.push(groundBody);

    let groundInst = new StaticBlock(pos.x, pos.y, size.x, size.y, groundBody);

    return groundInst;
}

var RayCastClosest = (function () {
    var def = {};

    def.reset = function () {
        def.hit = false;
        def.point = null;
        def.normal = null;
        def.body = null;
    };

    def.callback = function (fixture, point, normal, fraction) {
        var body = fixture.getBody();
        var userData = body.getUserData();
        if (userData) {
            if (userData === 0) {
                // By returning -1, we instruct the calling code to ignore this fixture and
                // continue the ray-cast to the next fixture.
                return -1.0;
            }
        }

        def.hit = true;
        def.point = point;
        def.normal = normal;
        def.body = body;

        // By returning the current fraction, we instruct the calling code to clip the ray and
        // continue the ray-cast to the next fixture. WARNING: do not assume that fixtures
        // are reported in order. However, by clipping, we can always get the closest fixture.
        return fraction;
    };

    return def;
})();

function getFixtureFromPosition(body, pos) {
    let fixtureLinkedList = body.getFixtureList();

    let fixtureArr = [];

    let currentFixture = fixtureLinkedList;
    while (currentFixture.m_next) {
        fixtureArr.push(currentFixture);
        currentFixture = currentFixture.m_next;
    }
    fixtureArr.push(currentFixture);

    for (let fix of fixtureArr) {
        let global_pos = Vec2(Math.floor(fix.getShape().m_centroid.x + body.getPosition().x), Math.floor(fix.getShape().m_centroid.y + body.getPosition().y));
        if (global_pos.x === pos.x && global_pos.y === pos.y) {
            return fix;
        }
    }


    for (let fix of fixtureArr) {
        console.error(fix);
    }
    console.error(`Can't find fixture at ${pos} in above list.`);
    return null;
}

// Assume the fixtures border each other
function doFixturesBorderOnFace(fix1, fix2, face) {
    let verts1 = fix1.getShape().m_vertices;
    let verts2 = fix2.getShape().m_vertices;

    if (verts1.length === 4 && verts2.length === 4) {
        return true;
    }

    if (face === "top") {
        if (
            verts1.includes(-0.5, 0.5) && verts1.includes(0.5, 0.5) &&
            verts2.includes(-0.5, -0.5) && verts2.includes(0.5, -0.5)
        ) {
            return true;
        }
    } else if (face === "left") {
        if (
            verts1.includes(0.5, -0.5) && verts1.includes(0.5, 0.5) &&
            verts2.includes(-0.5, -0.5) && verts2.includes(-0.5, 0.5)
        ) {
            return true;
        }
    }

    return false;
}

function start_effects() {}

// Converting coords from physics to smolpxl and visa-reversa
function physicsToSmolpxl(pos) {
    let position = [pos.x * 20, pos.y * 20];

    return position;
}

function smolpxlToPhysics(pos) {
    let position = Vec2(pos[0] / 20, pos[1] / 20);

    return position;
}

// Functions for UI changes

function changeCurrentShape(new_shape_id) {
    currentShape = new_shape_id
}

// Update
function update(gameScene) {
    if (true) {
        simTick++;
        let win = checkCondition(win_conditions);
        let lost = checkCondition(lose_conditions);

        if (win) {
            console.log("WON!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            gameScene.sim_on = false;
            gameScene.finished = true;
        }
        if (lost) {
            console.log("LOST!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            gameScene.sim_on = false;
            gameScene.finished = true;
        }

        for (let i = 0; i < 2; i++) {
            world.step(timeStep, velocityIterations, positionIterations);
            // if (main_car) {
            //     if (main_car.car.getLinearVelocity().x < 10) {
            //         main_car.joint.setMotorSpeed(500);
            //         main_car.joint.enableMotor(true);
            //     }
            // }

            if (effects.wind) {
                for (let i = 0; i < 16; i += 0.1) {
                    RayCastClosest.reset();
                    world.rayCast(
                        Vec2(-1, i),
                        Vec2(19, i),
                        RayCastClosest.callback
                    );

                    if (RayCastClosest.hit) {
                        RayCastClosest.body.applyForceToCenter(
                            Vec2(effects.wind / 10, 0)
                        );
                    }
                }
            }

            // for (let body of bodies) {
            //     if (effects.wind) {
            //         body.applyForceToCenter(Vec2(effects.wind,0));
            //     }
            // }

            let breakingJoints = [];

            for (let i = 0; i < joints.length; i++) {
                if (joints[i].m_type == "weld-joint") {
                    if (Vec2.lengthOf(joints[i].getReactionForce(1)) > 35) {
                        if (
                            joints[i].getBodyA().type != -1 &&
                            joints[i].getBodyB().type != -1
                        ) {
                            world.destroyJoint(joints[i]);
                            breakingJoints.push(i);
                        }
                    }
                }
            }

            for (let i of breakingJoints) {
                joints.splice(i, 1);
            }
        }
    }
}

function place_block(gameScene, pointer) {
    let collided = false;

    const position = gameScene.screenToPlanck(pointer);

    let snapped_pos = Vec2(
        Math.floor(position.x),
        Math.floor(position.y)
    );

    for (let shapeDef of shapeDefs[currentShape][0]) {
        let shape_pos = Vec2(
            snapped_pos.x + shapeDef[0].x,
            snapped_pos.y + shapeDef[0].y
        );

        for (let point of collisions_array) {
            if (
                shape_pos.x == point[0] &&
                shape_pos.y == point[1]
            ) {
                collided = true;
            }
        }
    }

    if (!collided) {
        const shape = createShape(
            gameScene,
            snapped_pos,
            currentShape,
            current_orientation
        );
        blocks.push(shape);
    }
}


/*
    for (let input of runningGame.input()) {
        if (input.name == "BUTTON1") {
            model.sim_on = true;
            joinTiles();
        }
        if (input.name == "LEFT_CLICK") {
            if (!model.sim_on) {
                let collided = false;

                let position = smolpxlToPhysics([input.x, input.y]);

                let snapped_pos = Vec2(
                    Math.floor(position.x),
                    Math.floor(position.y)
                );

                for (let shapeDef of shapeDefs[currentShape][0]) {
                    let shape_pos = Vec2(
                        snapped_pos.x + shapeDef[0].x,
                        snapped_pos.y + shapeDef[0].y
                    );

                    for (let point of collisions_array) {
                        if (
                            shape_pos.x == point[0] &&
                            shape_pos.y == point[1]
                        ) {
                            collided = true;
                        }
                    }
                }

                if (!collided) {
                    const shape = createShape(
                        gameScene,
                        snapped_pos,
                        currentShape,
                        current_orientation
                    );
                    blocks.push(shape);
                }
            }
        }
        if (input.name == "SELECT") {
            currentShape++;

            if (currentShape >= shapeDefs.length) {
                currentShape = 0;
            }
        }
        if (Number(input.name) != NaN) {
            if (shapeDefs[Number(input.name)]) {
                currentShape = Number(input.name);
                current_orientation = [0, false];
            }
        }
        if (input.name === "Q") {
            current_orientation[1] = false;
            if (current_orientation[0] === 90) {
                current_orientation[0] = 270;
            } else if (current_orientation[0] === 270) {
                current_orientation[0] = 90;
            }
        } else if (input.name === "E") {
            current_orientation[1] = true;
            if (current_orientation[0] === 90) {
                current_orientation[0] = 270;
            } else if (current_orientation[0] === 270) {
                current_orientation[0] = 90;
            }
        } else if (input.name === "W") {
            current_orientation[0] = 0;
        } else if (input.name === "S") {
            current_orientation[0] = 180;
        } else if (input.name === "D") {
            current_orientation[0] = 90;
        } else if (input.name === "A") {
            current_orientation[0] = 270;
        }
    }
*/

function joinTiles() {
    for (let y = 0; y < tiles.length; y++) {
        for (let x = 0; x < tiles[y].length; x++) {
            if (tiles[y][x] == -1) {
                continue;
            }


            if (y != 0) {
                if (
                    tiles[y - 1][x] != tiles[y][x] &&
                    tiles[y - 1][x] != -1
                ) {
                    if (doFixturesBorderOnFace(
                        getFixtureFromPosition(bodies[tiles[y][x]], Vec2(x,y)),
                        getFixtureFromPosition(bodies[tiles[y - 1][x]], Vec2(x,y - 1)),
                        "top")) {
                        let joint = createJoint(
                            bodies[tiles[y][x]],
                            bodies[tiles[y - 1][x]],
                            Vec2(x, y - 0.5)
                        );
                    }
                }
            }
            if (x != 0) {
                if (
                    tiles[y][x - 1] != tiles[y][x] &&
                    tiles[y][x - 1] != -1
                ) {
                    if (doFixturesBorderOnFace(
                        getFixtureFromPosition(bodies[tiles[y][x]], Vec2(x,y)),
                        getFixtureFromPosition(bodies[tiles[y][x - 1]], Vec2(x - 1,y)),
                        "left")) {
                        let joint = createJoint(
                            bodies[tiles[y][x]],
                            bodies[tiles[y][x - 1]],
                            Vec2(x - 0.5, y - 0.5)
                        );
                    }
                }
            }
        }
    }
}

// View
function view(screen, model) {
    for (let block of blocks) {
        let body = block.object;

        let vertices = [];

        let currentColor = block.colour;

        for (let f = body.getFixtureList(); f; f = f.getNext()) {
            if (f.getType() == "circle") {
                let shape = f.getShape();
                screen.setCircle(
                    physicsToSmolpxl(body.getPosition()),
                    physicsToSmolpxl(Vec2(shape.m_radius, shape.m_radius))[0],
                    "#000"
                );
            } else if (f.getType() == "polygon") {
                let shape = f.getShape();
                vertices = shape.m_vertices;
                let vertices_finished = [];

                for (let vertex_1 of vertices) {
                    let vertex_2 = body.getWorldPoint(vertex_1);
                    let vertex_finished = physicsToSmolpxl(vertex_2);
                    vertices_finished.push(vertex_finished);
                }

                screen.setPolygon(vertices_finished, currentColor);
            }
        }

        if (area) {
            let areaSmolpxl = [
                physicsToSmolpxl(Vec2(area.x1, area.y1)),
                physicsToSmolpxl(Vec2(area.x2, area.y1)),
                physicsToSmolpxl(Vec2(area.x2, area.y2)),
                physicsToSmolpxl(Vec2(area.x1, area.y2)),
            ];
            screen.setPolygon(areaSmolpxl, "#0000");
        }
    }
}

function setup(level, gameScene, dynObjDefs_result) {
    collisions_array = [];
    currentShape = 0;
    simTick = 0;
    bodies = [];
    joints = [];
    tiles = [];
    blocks = [];
    dynamic_objects = [];
    main_car = null;
    world = planck.World(gravity);

    dynObjDefs = jsonDynObjDefsToJS(dynObjDefs_result);

    let line = [];

    for (let y = 0; y < 20; y++) {
        line = [];
        for (let x = 0; x < 20; x++) {
            line.push(-1);
        }
        tiles.push(line);
    }

    let blocks_data = level.blocks;
    for (let block of blocks_data) {
        if (block.static_block) {
            let static_block = createGround(
                gameScene,
                Vec2(block.static_block.x, block.static_block.y),
                Vec2(block.static_block.w, block.static_block.h)
            );

            blocks.push(static_block);
        } else if (block.shape_block) {
            let shape_block = createShape(
                gameScene,
                Vec2(block.shape_block.x, block.shape_block.y),
                block.shape_block.id,
                [block.shape_block.rotation, block.shape_block.reflected]
            );

            blocks.push(shape_block);
        }
    }

    let dynamic_objects_data = level.dynamic_objects;
    for (let dynamic_object of dynamic_objects_data) {
        for (let dod of dynObjDefs) {
            if (dod.name === Object.keys(dynamic_object)[0]) {
                let new_dynObj = createDynObj(
                    gameScene,
                    dod,
                    dynamic_object[dod.name]
                );

                if (dod.type === "car") {
                    if (!main_car) {
                        main_car = new_dynObj;
                    }
                }

                dynamic_objects.push(new_dynObj);
            }
        }
        // if (dynamic_object.car) {
        //   let new_car = createCar(
        //     Vec2(dynamic_object.car.x, dynamic_object.car.y),
        //     dynamic_object.car.w_density,
        //     dynamic_object.car.b_density,
        //     dynamic_object.car.w_density,
        //     Vec2(dynamic_object.car.b_width, dynamic_object.car.b_height)
        //   );

        //   if (!main_car) {
        //     main_car = new_car;
        //   }

        //   dynamic_objects.push(new_car);
        // }
    }

    /*   // Making the ground
    let ground = createGround(Vec2(10.0, 17.5), Vec2(20.0, 5.0));
    let wall1 = createGround(Vec2(1.0, 13), Vec2(2.0, 4.0));
    let wall2 = createGround(Vec2(19.0, 13), Vec2(2.0, 4.0));
  
    let carlist = createCar(Vec2(4, 9), 1.0, 1.0, 1.0, Vec2(3.0, 2.0));
    car = carlist[0];
    axle = carlist[1];
  
    // Testing
    createShape(Vec2(2.5, 13.5), 2);
    createShape(Vec2(2.5, 11.5), 2);
    createShape(Vec2(16.5, 13.5), 2);
    createShape(Vec2(16.5, 11.5), 2);
  
    createShape(Vec2(4.5, 11.5), 3);
    createShape(Vec2(6.5, 11.5), 3);
    createShape(Vec2(8.5, 11.5), 3);
    createShape(Vec2(10.5, 11.5), 3);
    createShape(Vec2(12.5, 11.5), 3);
    createShape(Vec2(14.5, 11.5), 3); */

    // Conditions
    win_conditions = jsonConditionsToObjects(level.win_conditions);
    lose_conditions = jsonConditionsToObjects(level.lose_conditions);

    // Load effects
    effects = level.effects;
}

async function main() {
    const game = new Smolpxl.Game();

    game.showSmolpxlBar();
    game.setSize(screen_size[0], screen_size[1]);
    game.setBorderColor(Smolpxl.colors.BLACK);
    game.setBackgroundColor(Smolpxl.colors.WHITE);
    game.setTitle("Box Stacker");

    // Shape Defs
    let sd_response = await fetch("./shape_def.json", { cache: "no-cache" });
    let sd_result = await sd_response.json();

    shapeDefs = jsonShapeDefsToJS(sd_result);

    // Open a level
    let level_response = await fetch("./level.json", { cache: "no-cache" });
    let level = await level_response.json();

    // Dynamic Object Defs
    let dynObjDefs_response = await fetch(
        "./dynamic_object_def.json",
        { cache: "no-cache" },
    );
    let dynObjDefs_result = await dynObjDefs_response.json();

    setup(level, null, dynObjDefs_result);

    // Start the game
    game.start("box-stacker", startModel(), view, update);

    start_effects();
}

// Definitions
const screen_size = [400, 400];
const Vec2 = planck.Vec2;
const gravity = Vec2(0.0, 10.0);
const timeStep = 1.0 / 60.0;
const velocityIterations = 6;
const positionIterations = 2;

// Global Variables
let collisions_array;

let shapeDefs;

let dynObjDefs = [];

let currentShape;

let simTick;

// The list of bodies
let bodies;

// The list of joints
let joints;

// The grid of tiles
let tiles;

// The list of blocks
let blocks;

// The list of dynamic objects
let dynamic_objects;
let main_car;

let current_orientation = [0, false];

// Areas
let area;

// The Planck world
let world;

// Declaring Conditions
let win_conditions;
let lose_conditions;

let effects;

//main();
